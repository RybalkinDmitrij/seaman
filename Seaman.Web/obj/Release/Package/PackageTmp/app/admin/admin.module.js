﻿(function() {
    angular.module("seaman.admin", [
        "admin.collectionMethod",
        "admin.comment",
        "admin.physician",
        "admin.storage",
        "admin.user",
        "admin.extractReason"
    ]);
})();